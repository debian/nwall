nwall (1.32+debian-10) unstable; urgency=medium

  * QA upload.
  * d/copyright: Convert to machine-readable format.
  * Codify the current (unnecessary) repack rules.

 -- Bastian Germann <bage@debian.org>  Thu, 14 Nov 2024 21:30:05 +0000

nwall (1.32+debian-9) unstable; urgency=medium

  * QA upload.
  * Migrate nwall to use `debputy` to avoid requiring (fake)root
    when building the package.

 -- Niels Thykier <niels@thykier.net>  Fri, 08 Nov 2024 13:56:07 +0000

nwall (1.32+debian-8) unstable; urgency=medium

  * QA upload.

  * Added 02-free-include.patch to fix build problem with recent gcc.
  * Added 03-no-stderr-segfault.patch to fix segfault when stderr is /dev/null
    (Closes: #674678).

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 30 Jun 2024 08:51:39 +0200

nwall (1.32+debian-7) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 3.9.3.1 to 4.7.0.
  * Use wrap-and-sort -at for debian control files.
  * Replaced obsolete libncurses5-dev build dependency with libncurses-dev.
  * Drop fields with obsolete URLs.
  * Switched to debhelper compat level 13 and rewrite build to simple dh
    (Closes: #1011496).

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 29 Jun 2024 14:46:42 +0200

nwall (1.32+debian-6) unstable; urgency=medium

  * QA Upload.
  * Fix FTCBFS: Use Host compiler.

 -- Nilesh Patra <nilesh@debian.org>  Sat, 09 Jul 2022 21:19:22 +0530

nwall (1.32+debian-5) unstable; urgency=medium

  [ Andreas Beckmann ]
  * QA upload.
  * Set Maintainer to Debian QA Group.  (See: #784187)

  [ Jari Aalto ]
  * Remove deprecated dpatch and upgrade to packaging format "3.0 quilt"
    (Closes: #667639).
  * Update to Standards-Version to 3.9.3.1 and debhelper to 9.
    (Closes: #965750)
  * Add homepage field to control file.
  * Add build-arch and build-indep targets; use dh_prep in rules file.
  * Fix copyright-refers-to-symlink-license (Lintian).

 -- Andreas Beckmann <anbe@debian.org>  Wed, 13 Jan 2021 14:45:13 +0100

nwall (1.32+debian-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Do not ship /var/lock/nwall. Lack of this directory on wheezy systems with
    a ramdisk for /run does not seem to have caused any problems, so do not
    add an initscript to create it. Using /var/lock for opt-out flag files
    seems flawed anyway, since they won't survive a reboot.  (Closes: #768273)

 -- Andreas Beckmann <anbe@debian.org>  Thu, 06 Nov 2014 11:35:52 +0100

nwall (1.32+debian-4.1) unstable; urgency=low

  * Non maintainer upload.
  * Build-depend on libreadline-dev | libreadline5-dev.
    Closes: #553817. LP: #832885.

 -- Matthias Klose <doko@debian.org>  Fri, 02 Sep 2011 12:25:42 +0200

nwall (1.32+debian-4) unstable; urgency=low

  * New maintainer (Closes: #407633)

 -- Luis Uribe <acme@eviled.org>  Fri, 23 Mar 2007 22:34:34 -0500

nwall (1.32+debian-3) unstable; urgency=low

  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Jan 2007 10:47:00 +0100

nwall (1.32+debian-2) unstable; urgency=low

  * New email address.
  * Bumped policy version.
  * Now using dpatch for upstream modifications.

 -- Daniel Baumann <daniel@debian.org>  Thu, 17 Aug 2006 11:52:00 +0200

nwall (1.32+debian-1) unstable; urgency=low

  * Rebuild upstream-tarball without conflicting debian-directory.
  * Rebuild against libreadline5 (Closes: #326313).
  * Updated debian/ according newer debhelber templates.
  * Now updating config.{guess,sub} via diff.gz.
  * Added lintian overrides suppressing permission complains.
  * Bumped policy version.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Tue,  4 Oct 2005 10:09:00 +0200

nwall (1.32-4) unstable; urgency=low

  * New maintainer (Closes: #275777).
  * debian/conffiles removed (was empty).
  * debian/control: formal changes.
  * debian/copyright: formal changes.
  * debian/logrotate.d: renamed to nwall.logratate and added dh_installlogrotate in rules.
  * debian/rules: replaced `pwd` with $(CURDIR).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Tue,  7 Dec 2004 21:47:00 +0100

nwall (1.32-3) unstable; urgency=low

  * QA upload.
  * Fix segfault when encountering lockfiles from users without passwd entry
    (Closes: #150669).
  * Remove usr/sbin from debian/dirs.
  * Cosmetic debian/copyright fix.
  * Update watch file to version=2 and new upstream URL.
  * Cleanup debian/rules.
  * Bump Standards-Version; debhelper 4.

 -- Christoph Berg <cb@df7cb.de>  Sat, 27 Nov 2004 18:46:29 +0100

nwall (1.32-2) unstable; urgency=low

  * Orphaned.  Maintainer set to Debian QA Group <packages@qa.debian.org>.

 -- Carlos Laviola <claviola@debian.org>  Sun, 10 Oct 2004 04:48:23 -0300

nwall (1.32-1) unstable; urgency=low

  * "New" upstream release.
  * Got rid of emacs tags in changelog.

 -- Carlos Laviola <claviola@debian.org>  Wed, 20 Aug 2003 22:25:27 -0300

nwall (1.31-1) unstable; urgency=low

  * New upstream release. <-- emad

 -- Carlos Laviola <claviola@debian.org>  Sun, 14 Apr 2002 18:57:55 -0300

nwall (1.30-1) unstable; urgency=low

  * First official Debian version.
  * Fixed up the versioning.
  * Included support for log rotation using logrotate.
  * debian/control: Added Build-Depends: libreadline4-dev,
    debhelper (>> 1.0).

 -- Carlos Laviola <claviola@debian.org>  Fri, 28 Dec 2001 19:20:13 -0200

nwall (1.30) unstable; urgency=low

  * added emad's -s option

 -- Nick Moffitt <nick@zork.net>  Sun,  8 Apr 2001 13:54:52 -0700

nwall (1.26-2) unstable; urgency=low

  * added conffiles

 -- Nick Moffitt <nick@zork.net>  Sat, 10 Mar 2001 15:24:42 -0800

nwall (1.26-1) unstable; urgency=low

  * cleaned up some make dist stuff

 -- Nick Moffitt <nick@zork.net>  Sat, 10 Mar 2001 14:32:12 -0800

nwall (1.26) unstable; urgency=low

  * oops, wrong lock dir

 -- Nick Moffitt <nick@zork.net>  Sat, 10 Mar 2001 14:15:29 -0800

nwall (1.25-1) unstable; urgency=low

  * oops, build had wrong strings for log file!

 -- Nick Moffitt <nick@zork.net>  Sat, 10 Mar 2001 02:23:17 -0800

nwall (1.25) unstable; urgency=low

  * finally got permissions and build right

 -- Nick Moffitt <nick@zork.net>  Sat, 10 Mar 2001 02:10:20 -0800

nwall (1.20) unstable; urgency=low

  * Fixed a number of bugs

 -- Nick Moffitt <nick@zork.net>  Fri,  9 Mar 2001 20:01:02 -0800

nwall (1.10-1) unstable; urgency=low

  * Initial Release.

 -- Nick Moffitt <nick@zork.net>  Sun, 31 Dec 2000 17:50:21 -0800
